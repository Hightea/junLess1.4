FROM debian:9 AS nginxbuilder
RUN apt-get update && apt-get install -y procps make wget gcc libpcre3 libpcre3-dev zlib1g zlib1g-dev \
&& wget  http://nginx.org/download/nginx-1.0.5.tar.gz \
&& tar xvfz nginx-1.0.5.tar.gz \
&& cd nginx-1.0.5 \ 
&& ./configure && make && make install

FROM debian:9
WORKDIR /usr/local/nginx/sbin
COPY --from=nginxbuilder /usr/local/nginx /usr/local/nginx 
RUN chmod +x ./nginx
CMD ["./nginx", "-g", "daemon off;"]

